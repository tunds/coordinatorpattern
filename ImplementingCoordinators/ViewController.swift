//
//  ViewController.swift
//  ImplementingCoordinators
//
//  Created by Tunde on 15/05/2019.
//  Copyright © 2019 Degree 53 Limited. All rights reserved.
//

import UIKit

class ViewController: UIViewController, Storyboarded {

    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func buyDidTouch(_ sender: Any) {
        coordinator?.buySubscription(to: 10)
    }
    
    @IBAction func createDidTouch(_ sender: Any) {
        coordinator?.createAccount()
    }
}

