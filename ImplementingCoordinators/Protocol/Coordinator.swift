//
//  Coordinator.swift
//  ImplementingCoordinators
//
//  Created by Tunde on 15/05/2019.
//  Copyright © 2019 Degree 53 Limited. All rights reserved.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
    func childDidFinish(_ child: Coordinator?)
}

extension Coordinator {
    
    func childDidFinish(_ child: Coordinator?) {
        guard let child = child else { return }
        self.childCoordinators.enumerated().forEach { if $0.element === child { childCoordinators.remove(at: $0.offset) }  }
    }
}
