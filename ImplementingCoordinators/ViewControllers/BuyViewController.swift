//
//  BuyViewController.swift
//  ImplementingCoordinators
//
//  Created by Tunde on 15/05/2019.
//  Copyright © 2019 Degree 53 Limited. All rights reserved.
//

import UIKit

class BuyViewController: UIViewController, Storyboarded {

    weak var coordinator: BuyCoordinator?
    var selectedProduct = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("The selected product is: \(selectedProduct)")
    }
}
